/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50648
 Source Host           : localhost:3306
 Source Schema         : tp6layui

 Target Server Type    : MySQL
 Target Server Version : 50648
 File Encoding         : 65001

 Date: 02/07/2021 15:29:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sysdepartment
-- ----------------------------
DROP TABLE IF EXISTS `sysdepartment`;
CREATE TABLE `sysdepartment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT 0,
  `project_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` char(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '可用',
  `create_time` datetime(0) NOT NULL,
  `topids` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `sunids` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `pid`(`pid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 38 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysdepartment
-- ----------------------------
INSERT INTO `sysdepartment` VALUES (1, 0, 1, '系统管理员', '可用', '2015-11-15 14:00:39', NULL, '');
INSERT INTO `sysdepartment` VALUES (19, 25, 1, '微信部', '可用', '2017-02-21 14:00:37', NULL, '');
INSERT INTO `sysdepartment` VALUES (3, 25, 1, '网路部', '可用', '2015-11-15 14:01:13', NULL, '');
INSERT INTO `sysdepartment` VALUES (27, 3, 1, '项目部', '可用', '2017-02-23 08:54:27', NULL, '');
INSERT INTO `sysdepartment` VALUES (21, 19, 1, '微信一组', '可用', '2017-02-21 14:28:02', NULL, '');
INSERT INTO `sysdepartment` VALUES (22, 19, 1, '微信二组', '可用', '2017-02-21 14:28:27', NULL, '');
INSERT INTO `sysdepartment` VALUES (23, 19, 1, '微信三组', '可用', '2017-02-21 14:41:37', NULL, '');
INSERT INTO `sysdepartment` VALUES (24, 1, 1, '律师部门', '可用', '2017-02-23 08:51:26', NULL, '');
INSERT INTO `sysdepartment` VALUES (25, 1, 1, '公司添加数据部', '可用', '2017-02-23 08:52:36', NULL, '');
INSERT INTO `sysdepartment` VALUES (26, 19, 1, '微信四组', '可用', '2017-02-23 08:53:31', NULL, '');
INSERT INTO `sysdepartment` VALUES (20, 24, 1, '咨询医生', '可用', '2017-02-21 14:02:30', NULL, '');
INSERT INTO `sysdepartment` VALUES (12, 1, 1, '前台', '可用', '2016-08-04 14:18:03', NULL, '');
INSERT INTO `sysdepartment` VALUES (28, 3, 1, '客服部', '可用', '2017-02-23 08:54:40', NULL, '');
INSERT INTO `sysdepartment` VALUES (29, 1, 1, '人事部', '可用', '2017-02-23 09:08:26', NULL, '');
INSERT INTO `sysdepartment` VALUES (17, 1, 1, '公司经营', '可用', '2017-02-21 11:39:23', NULL, '');
INSERT INTO `sysdepartment` VALUES (31, 25, 1, '企划部', '可用', '2017-03-01 11:43:07', NULL, '');
INSERT INTO `sysdepartment` VALUES (32, 1, 1, '财务部', '可用', '2018-03-06 10:28:02', NULL, '');
INSERT INTO `sysdepartment` VALUES (33, 1, 1, '技术部', '可用', '2018-04-11 15:43:51', NULL, '');
INSERT INTO `sysdepartment` VALUES (34, 0, 2, '总经办', '可用', '2020-06-12 13:57:58', NULL, NULL);
INSERT INTO `sysdepartment` VALUES (35, 34, 2, 'dsfg', '可用', '2020-06-12 14:05:06', NULL, NULL);
INSERT INTO `sysdepartment` VALUES (36, 34, 2, 'sdfg', '可用', '2020-06-12 14:06:59', NULL, NULL);
INSERT INTO `sysdepartment` VALUES (37, 0, 2, 'fff', '可用', '2020-06-12 14:09:08', NULL, NULL);

-- ----------------------------
-- Table structure for syslog
-- ----------------------------
DROP TABLE IF EXISTS `syslog`;
CREATE TABLE `syslog`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '操作记录主键',
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '操作人',
  `username` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作人姓名',
  `node` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作节点',
  `type` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作类型',
  `ip` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '记录操作IP,省市,等信息',
  `remark` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  `create_time` datetime(0) NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sysmenu
-- ----------------------------
DROP TABLE IF EXISTS `sysmenu`;
CREATE TABLE `sysmenu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `name` char(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT 1,
  `pid` int(11) NOT NULL DEFAULT 0,
  `ismenu` tinyint(1) NOT NULL DEFAULT 0,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `sort` tinyint(11) NOT NULL DEFAULT 0,
  `condition` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `checkarr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  INDEX `title`(`title`) USING BTREE,
  INDEX `ismenu`(`ismenu`) USING BTREE,
  INDEX `pid`(`pid`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 257 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统菜单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysmenu
-- ----------------------------
INSERT INTO `sysmenu` VALUES (1, '后台首页', 'admin/index/index', 1, 5, 0, 'layui-icon layui-icon-light', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (2, '查看', 'admin/staff.staff/chakan', 1, 116, 0, '阿斯蒂芬', 1, 2, 'SD敢达阿斯蒂芬', '0');
INSERT INTO `sysmenu` VALUES (3, '左边', 'Layout/west', 1, 1, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (4, '上边', 'Layout/north', 1, 3, 0, '', 1, 47, '', '0');
INSERT INTO `sysmenu` VALUES (5, '系统', '系统', 1, 0, 1, 'layui-icon layui-icon-windows', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (6, '用户管理', 'admin/system.sysuser/index', 2, 5, 1, 'layui-icon-username', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (7, '用户添加', 'admin/system.sysuser/add', 2, 6, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (8, '菜单管理', 'admin/system.sysmenu/index', 1, 5, 1, 'layui-icon-template-1', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (9, '角色管理', 'admin/system.sysrole/index', 1, 5, 1, 'layui-icon-user', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (10, '节点获取所有', 'admin/system.sysmenu/view', 1, 8, 1, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (11, '节点编辑', 'admin/system.sysmenu/edit', 1, 8, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (12, '节点添加', 'admin/system.sysmenu/add', 1, 8, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (13, '节点删除', 'admin/system.sysmenu/delete', 1, 8, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (14, '用户获取所有', 'admin/system.sysuser/view', 1, 6, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (15, '用户编辑', 'admin/system.sysuser/edit', 1, 6, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (16, '用户删除', 'admin/system.sysuser/delete', 1, 6, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (17, '用户公共组件', 'User/viewWidget', 1, 174, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (18, '角色获取所有', 'admin/system.sysrole/view', 1, 9, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (19, '角色添加', 'admin/system.sysrole/add', 1, 9, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (20, '角色编辑', 'admin/system.sysrole/edit', 1, 9, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (21, '角色删除', 'admin/system.sysrole/delete', 1, 9, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (22, '工资级别管理', 'Wagegrade/index', 1, 174, 1, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (23, '工资级别获取所有', 'Wagegrade/view', 1, 22, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (24, '工资级别添加', 'Wagegrade/add', 1, 22, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (25, '工资级别编辑', 'Wagegrade/edit', 1, 22, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (26, '工资级别删除', 'Wagegrade/delete', 1, 22, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (27, '服务器获取by服务商id组件', 'Fuwuqi/viewWidgetByFuwushangid', 1, 22, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (28, '人员合同管理', 'admin/staff.staffdeal/index', 1, 171, 0, '', 1, 12, '', '0');
INSERT INTO `sysmenu` VALUES (29, '手机卡获取所有', 'Zctelcard/view', 1, 28, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (30, '添加', 'admin/staff.staffdeal/add', 1, 28, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (31, '编辑', 'admin/staff.staffdeal/edit', 1, 28, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (32, '删除', 'admin/staff.staffdeal/delete', 1, 28, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (33, '服务商获取组件', 'Fuwushang/viewWidget', 1, 28, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (34, '服务商byid获取组件', 'Fuwushang/viewWidgetById', 1, 28, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (35, '媒体大类管理', 'admin/system.sysmediatype/index', 1, 5, 1, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (36, '公告获取所有', 'Gonggao/view', 1, 174, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (37, '添加', 'admin/system.sysmediatype/add', 1, 35, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (38, '编辑', 'admin/system.sysmediatype/edit', 1, 35, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (39, '删除', 'admin/system.sysmediatype/delete', 1, 35, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (40, '系统信息', 'Layout/main', 1, 1, 0, '', 1, 0, '体育IT与i', '0');
INSERT INTO `sysmenu` VALUES (41, '图表管理', 'Pic/index', 1, 5, 0, 'layui-icon-loading', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (42, '图表high', 'Pic/high', 1, 41, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (43, '图表pic', 'Pic/pic', 1, 41, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (44, '评论管理', 'Pinglun/index', 1, 5, 0, 'layui-icon-loading', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (45, '评论获取所有', 'Pinglun/view', 1, 44, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (46, '评论添加', 'Pinglun/add', 1, 44, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (47, '评论编辑', 'Pinglun/edit', 1, 44, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (48, '评论删除', 'Pinglun/delete', 1, 44, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (49, '评论审核', 'Pinglun/shenhe', 1, 44, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (50, '评论禁用', 'Pinglun/jinyong', 1, 44, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (51, '机构管理', 'admin/system.sysproject/index', 1, 5, 1, 'layui-icon-list', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (52, '添加', 'admin/system.sysproject/add', 1, 51, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (53, '编辑', 'admin/system.sysproject/edit', 1, 51, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (54, '删除', 'admin/system.sysproject/delete', 1, 51, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (55, '根据登录人员获取信息组件', 'Project/viewWidget', 1, 51, 0, '', 1, 0, 'javascript:void(0)', '0');
INSERT INTO `sysmenu` VALUES (56, 'QQ管理', 'Qq/index', 1, 174, 1, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (59, 'QQ编辑', 'Qq/edit', 1, 56, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (60, 'QQ删除', 'Qq/delete', 1, 56, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (61, '人员资质管理', 'admin/staff.staffzizhi/index', 1, 171, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (62, '账号粉丝获取所有', 'Zczhanghaofensi/view', 1, 61, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (63, '添加', 'admin/staff.staffzizhi/add', 1, 61, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (64, '编辑', 'admin/staff.staffzizhi/edit', 1, 61, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (65, '删除', 'admin/staff.staffzizhi/delete', 1, 61, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (66, '人员异动管理', 'admin/staff.staffchanges/index', 1, 171, 0, '', 1, 11, '', '0');
INSERT INTO `sysmenu` VALUES (67, '手机管理获取所有', 'Zctel/view', 1, 66, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (68, '添加', 'admin/staff.staffchanges/add', 1, 66, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (69, '编辑', 'admin/staff.staffchanges/edit', 1, 66, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (70, '删除', 'admin/staff.staffchanges/delete', 1, 66, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (72, '竞价账号管理', 'Zcjingjianumber/index', 1, 172, 1, '', 1, 16, '', '0');
INSERT INTO `sysmenu` VALUES (73, '竞价账号获取所有', 'Zcjingjianumber/view', 1, 72, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (74, '竞价账号添加', 'Zcjingjianumber/add', 1, 72, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (75, '竞价账号编辑', 'Zcjingjianumber/edit', 1, 72, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (76, '竞价账号删除', 'Zcjingjianumber/delete', 1, 72, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (77, '网站加速管理', 'Webjiasu/index', 1, 174, 1, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (78, '网站加速获取所有', 'Webjiasu/view', 1, 77, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (79, '网站加速添加', 'Webjiasu/add', 1, 77, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (80, '网站加速编辑', 'Webjiasu/edit', 1, 77, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (81, '网站加速删除', 'Webjiasu/delete', 1, 77, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (82, '办公室管理', 'Zcoffice/index', 1, 172, 1, '', 1, 14, '', '0');
INSERT INTO `sysmenu` VALUES (83, '办公室管理获取所有', 'Zcoffice/view', 1, 82, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (84, '办公室管理添加', 'Zcoffice/add', 1, 82, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (85, '办公室管理编辑', 'Zcoffice/edit', 1, 82, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (86, '办公室管理删除', 'Zcoffice/delete', 1, 82, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (87, '类型管理', 'admin/system.syscasetype/index', 1, 5, 0, '', 1, 15, '', '0');
INSERT INTO `sysmenu` VALUES (88, '域名获取所有', 'Zcdomain/view', 1, 174, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (89, '添加', 'admin/system.syscasetype/add', 1, 87, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (90, '编辑', 'admin/system.syscasetype/edit', 1, 87, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (91, '删除', 'admin/system.syscasetype/delete', 1, 87, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (96, '服务器维护', '服务器维护', 1, 174, 0, '', 1, 2, '', '0');
INSERT INTO `sysmenu` VALUES (97, '账号粉丝数据', 'Zczhanghao/fensi', 1, 181, 0, '', 1, 0, '', '0');
INSERT INTO `sysmenu` VALUES (98, '其他工具', '其他工具', 1, 174, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (99, '后台portal', 'Layout/portal', 1, 1, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (100, '后台首页公告', 'Layout/gonggao', 1, 1, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (101, '案件', '案件', 1, 0, 0, 'layui-icon-read', 1, 10, '', '0');
INSERT INTO `sysmenu` VALUES (102, '案件管理', 'admin/cases.case/index', 1, 101, 1, '', 1, 9, '', '0');
INSERT INTO `sysmenu` VALUES (104, '添加', 'admin/cases.case/add', 1, 102, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (105, '编辑', 'admin/cases.case/edit', 1, 102, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (106, '删除', 'admin/cases.case/delete', 1, 102, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (107, '勾到', 'admin/cases.case/goudao', 1, 102, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (108, '回访', 'admin/cases.case/huifang', 1, 102, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (109, '竞价渠道', 'admin/cases.case/jingjiaqudao', 1, 102, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (110, '部门管理', 'admin/system.sysdepartment/index', 1, 5, 1, 'layui-icon-group', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (112, '部门添加', 'admin/system.sysdepartment/add', 1, 110, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (113, '部门编辑', 'admin/system.sysdepartment/edit', 1, 110, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (114, '部门删除', 'admin/system.sysdepartment/delete', 1, 110, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (115, '用户管理重置密码', 'admin/system/sysuser/reset', 1, 6, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (116, '人员管理', 'admin/staff.staff/index', 1, 171, 1, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (117, '异动', 'admin/staff.staff/yidong', 1, 116, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (118, '人员添加', 'admin/staff.staff/add', 1, 116, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (119, '人员编辑', 'admin/staff.staff/edit', 1, 116, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (120, '人员删除', 'admin/staff.staff/delete', 1, 116, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (121, '合同', 'admin/staff.staff/hetong', 1, 116, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (122, '人员导出报表', 'Employee/daochuexcel', 1, 116, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (123, '资质', 'admin/staff.staff/zizhi', 1, 116, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (124, '人员离职管理', 'Employeelizhi/index', 1, 171, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (125, '人员离职获取所有', 'Employeelizhi/view', 1, 124, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (126, '人员离职编辑', 'Employeelizhi/edit', 1, 124, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (127, '人员离职删除', 'Employeelizhi/delete', 1, 124, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (128, '岗位管理', 'Gangwei/index', 1, 174, 1, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (129, '岗位获取所有', 'Gangwei/view', 1, 128, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (130, '岗位添加', 'Gangwei/add', 1, 128, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (131, '岗位编辑', 'Gangwei/edit', 1, 128, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (132, '岗位删除', 'Gangwei/delete', 1, 128, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (133, '岗位功能组件', 'Gangwei/viewWidget', 1, 128, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (134, '案件谈案管理', 'admin/cases.casetalk/index', 1, 101, 1, '', 1, 10, '', '0');
INSERT INTO `sysmenu` VALUES (135, '谈案', 'admin/cases.case/tanan', 1, 102, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (136, '添加', 'admin/cases.casetalk/add', 1, 134, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (137, '编辑', 'admin/cases.casetalk/edit', 1, 134, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (138, '删除', 'admin/cases.casetalk/delete', 1, 134, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (139, '联系人', 'admin/cases.case/lianxiren', 1, 102, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (140, '转移', 'admin/cases.case/zhuanyi', 1, 102, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (141, '媒体管理', 'admin/system.sysmedia/index', 1, 5, 1, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (142, '楼层获取所有', 'Louceng/view', 1, 1, 1, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (143, '添加', 'admin/system.sysmedia/add', 1, 141, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (144, '编辑', 'admin/system.sysmedia/edit', 1, 141, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (145, '删除', 'admin/system.sysmedia/delete', 1, 141, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (146, '楼层公共组件', 'Louceng/viewWidget', 1, 174, 1, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (147, '电脑配置管理', 'Peizhi/index', 1, 174, 1, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (148, '配置获取所有', 'Peizhi/view', 1, 147, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (149, '配置添加', 'Peizhi/add', 1, 147, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (150, '配置编辑', 'Peizhi/edit', 1, 147, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (151, '配置删除', 'Peizhi/delete', 1, 147, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (152, '配置公共组件', 'Peizhi/viewWidget', 1, 147, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (153, '电脑设备管理', 'Shebei/index', 1, 172, 1, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (154, '设备添加', 'Shebei/add', 1, 153, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (155, '设备编辑', 'Shebei/edit', 1, 153, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (156, '设备删除', 'Shebei/delete', 1, 153, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (157, '设备获取所有', 'Shebei/view', 1, 153, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (158, '设备公共组件', 'Shebei/viewWidget', 1, 153, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (159, '培训', 'admin/staff.staff/peixun', 1, 116, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (160, '设备by用户id公共组件', 'Shebei/viewWidgetEmployeeid', 1, 153, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (161, '设备导出报表', 'Shebei/daochuexcel', 1, 153, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (162, '电脑设备使用情况', 'Shebeishiyong/index', 1, 172, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (163, '设备使用获取所有', 'Shebeishiyong/view', 1, 162, 1, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (164, '设备使用编辑', 'Shebeishiyong/edit', 1, 162, 1, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (165, '案件联系人管理', 'admin/cases.casecontact/index', 1, 101, 0, '', 1, 7, '', '0');
INSERT INTO `sysmenu` VALUES (166, '获取所有', 'admin/uuys/uuysarctype/view', 1, 17, 1, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (167, '添加', 'admin/cases.casecontact/add', 1, 165, 1, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (168, '编辑', 'admin/cases.casecontact/edit', 1, 165, 1, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (169, '删除', 'admin/cases.casecontact/delete', 1, 165, 1, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (170, '宿舍公共组件', 'Sushe/viewWidget', 1, 98, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (171, '人事', '人事', 1, 0, 1, 'layui-icon-username', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (172, '资产管理', '资产管理', 1, 0, 0, 'layui-icon-rmb', 1, 3, '', '0');
INSERT INTO `sysmenu` VALUES (174, '基础信息', '基础信息', 1, 0, 0, 'layui-icon-component', 1, 9, '', '0');
INSERT INTO `sysmenu` VALUES (176, '系统日志', 'admin/system.syslog/index', 1, 5, 1, 'layui-icon-log', 1, 8, '', '0');
INSERT INTO `sysmenu` VALUES (177, '系统日志批量删除', 'admin/system.syslog/delete', 1, 176, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (178, '系统日志获取所有', 'Log/view', 1, 176, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (179, '在线用户', 'Useronline/index', 1, 5, 1, 'layui-icon-friends', 1, 9, '', '0');
INSERT INTO `sysmenu` VALUES (180, '在线用户获取所有', 'Useronline/view', 1, 179, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (181, '账号管理', 'Zczhanghao/index', 1, 172, 1, '', 1, 10, '', '0');
INSERT INTO `sysmenu` VALUES (182, '账号管理获取所有', 'Zczhanghao/view', 1, 181, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (183, '账号管理添加', 'Zczhanghao/add', 1, 181, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (184, '账号管理编辑', 'Zczhanghao/edit', 1, 181, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (185, '账号管理删除', 'Zczhanghao/delete', 1, 181, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (186, '删除', 'admin/staff.staffpeixun/delete', 1, 190, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (187, '添加', 'admin/staff.staffpeixun/add', 1, 190, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (188, '编辑', 'admin/staff.staffpeixun/edit', 1, 190, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (189, '打印机管理获取所有', 'Zcprinter/view', 1, 190, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (190, '人员培训管理', 'admin/staff.staffpeixun/index', 1, 171, 0, '', 1, 13, '', '0');
INSERT INTO `sysmenu` VALUES (191, '服务器管理', 'Zcfuwuqi/index', 1, 172, 1, '', 1, 17, '', '0');
INSERT INTO `sysmenu` VALUES (192, '服务器管理获取所有', 'Zcfuwuqi/view', 1, 191, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (193, '服务器管理添加', 'Zcfuwuqi/add', 1, 191, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (194, '服务器管理删除', 'Zcfuwuqi/delete', 1, 191, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (195, '服务器管理编辑', 'Zcfuwuqi/edit', 1, 191, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (196, '电台视频管理', 'admin/uuys/uuysarcvedio/index', 1, 101, 1, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (197, '微信用户管理', 'admin/system/sysuserwx/index', 1, 5, 1, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (198, '获取所有', 'admin/uuys/uuysarcvedio/view', 1, 196, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (199, '添加', 'admin/uuys/uuysarcvedio/add', 1, 196, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (200, '编辑', 'admin/uuys/uuysarcvedio/edit', 1, 196, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (201, '菜品管理删除', 'Bccandan/delete', 1, 196, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (202, '获取所有', 'admin/system/sysuserwx/view', 1, 197, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (203, '报餐管理导出', 'Baocan/daochuexcel', 1, 197, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (204, '通知管理', 'Bcnotify/index', 1, 5, 1, '', 1, 5, '', '0');
INSERT INTO `sysmenu` VALUES (205, '通知管理获取所有', 'Bcnotify/view', 1, 204, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (206, '通知管理添加', 'Bcnotify/add', 1, 204, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (207, '通知管理编辑', 'Bcnotify/edit', 1, 204, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (208, '通知管理删除', 'Bcnotify/delete', 1, 204, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (209, '编辑', 'admin/system/sysuserwx/edit', 1, 197, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (210, '问题管理', 'Quiz/index', 1, 174, 1, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (211, '问题管理-获取所有', 'Quiz/view', 1, 210, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (212, '问题管理-添加', 'Quiz/add', 1, 210, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (213, '问题管理-编辑', 'Quiz/edit', 1, 210, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (214, '问题管理-删除', 'Quiz/delete', 1, 210, 0, '', 1, 1, '', '0');
INSERT INTO `sysmenu` VALUES (254, '是大法官是大法官', '上的风格是大法官', 1, 1, 1, '', 1, 1, '是大法官是大法官', '0');
INSERT INTO `sysmenu` VALUES (256, '测试哦', '水电费供货商电饭锅', 1, 3, 1, 'bnm,', 1, 0, 'bnm,', '0');

-- ----------------------------
-- Table structure for sysproject
-- ----------------------------
DROP TABLE IF EXISTS `sysproject`;
CREATE TABLE `sysproject`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `diseaseids` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mediatypeid` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mediaid` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `name`(`name`) USING BTREE,
  INDEX `addtime`(`create_time`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysproject
-- ----------------------------
INSERT INTO `sysproject` VALUES (1, '陕西华佑医疗集团', '1,3,6,7,11,48,75,82,110,111,112,113,114,115,116', '1,5,6,3', '1,3,8,19,20,21,22,24,26,28,29,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,88', '2015-11-01 14:12:33');
INSERT INTO `sysproject` VALUES (2, '西安现代妇产医院', NULL, NULL, NULL, '0000-00-00 00:00:00');
INSERT INTO `sysproject` VALUES (8, '医林互联医疗科技公司', '82,99,100,101,102,103,104,105,106,107,108,109,117', '1,5,6', '1,3,8,19,20,21,22,23,24,28,68,69,70,71,77,78,79,81,82,83,84,85,86,87', '2015-11-01 14:13:39');
INSERT INTO `sysproject` VALUES (21, '天颐堂中医医院', NULL, NULL, NULL, '2020-05-20 12:40:46');
INSERT INTO `sysproject` VALUES (24, '西安现代医美', NULL, NULL, NULL, '2020-05-20 15:35:43');

-- ----------------------------
-- Table structure for sysproject_user
-- ----------------------------
DROP TABLE IF EXISTS `sysproject_user`;
CREATE TABLE `sysproject_user`  (
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of sysproject_user
-- ----------------------------
INSERT INTO `sysproject_user` VALUES (1, 167);
INSERT INTO `sysproject_user` VALUES (1, 168);
INSERT INTO `sysproject_user` VALUES (24, 139);
INSERT INTO `sysproject_user` VALUES (1, 180);
INSERT INTO `sysproject_user` VALUES (8, 177);
INSERT INTO `sysproject_user` VALUES (2, 177);
INSERT INTO `sysproject_user` VALUES (1, 170);
INSERT INTO `sysproject_user` VALUES (1, 182);
INSERT INTO `sysproject_user` VALUES (1, 181);
INSERT INTO `sysproject_user` VALUES (1, 173);
INSERT INTO `sysproject_user` VALUES (1, 169);
INSERT INTO `sysproject_user` VALUES (1, 176);
INSERT INTO `sysproject_user` VALUES (1, 166);
INSERT INTO `sysproject_user` VALUES (1, 179);
INSERT INTO `sysproject_user` VALUES (1, 178);
INSERT INTO `sysproject_user` VALUES (1, 183);
INSERT INTO `sysproject_user` VALUES (21, 139);
INSERT INTO `sysproject_user` VALUES (8, 139);
INSERT INTO `sysproject_user` VALUES (2, 139);
INSERT INTO `sysproject_user` VALUES (1, 139);

-- ----------------------------
-- Table structure for sysrole
-- ----------------------------
DROP TABLE IF EXISTS `sysrole`;
CREATE TABLE `sysrole`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT 0,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `rules` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `checkarr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `title`(`title`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysrole
-- ----------------------------
INSERT INTO `sysrole` VALUES (2, 12, '咨询主管', 1, '0', 'rty', '0');
INSERT INTO `sysrole` VALUES (7, 23, '人事主管', 1, NULL, 'erty ', '0');
INSERT INTO `sysrole` VALUES (10, 13, '人事', 1, NULL, '飞规划局', '0');
INSERT INTO `sysrole` VALUES (11, 13, '公司运营', 1, NULL, '儿童医院', '0');
INSERT INTO `sysrole` VALUES (12, 23, '咨询', 1, NULL, '认同与', '0');
INSERT INTO `sysrole` VALUES (13, 23, '公司经营', 1, NULL, '', '0');
INSERT INTO `sysrole` VALUES (18, 13, '财务', 1, NULL, NULL, '0');
INSERT INTO `sysrole` VALUES (21, 13, '技术', 1, NULL, '', '0');
INSERT INTO `sysrole` VALUES (20, 23, '竞价', 1, NULL, NULL, '0');
INSERT INTO `sysrole` VALUES (22, 20, '竞价主管', 1, NULL, NULL, '0');
INSERT INTO `sysrole` VALUES (23, 0, '超级管理员', 1, NULL, '', '0');
INSERT INTO `sysrole` VALUES (24, 7, 'yui', 1, NULL, 'tyui', '0');
INSERT INTO `sysrole` VALUES (25, 24, 'rtyu', 1, NULL, 'rtyu', '0');
INSERT INTO `sysrole` VALUES (26, 7, '人事专员', 1, NULL, '是', '0');
INSERT INTO `sysrole` VALUES (27, 11, 'yuityui', 1, NULL, 'tyuityuityui', '0');

-- ----------------------------
-- Table structure for sysrole_menu
-- ----------------------------
DROP TABLE IF EXISTS `sysrole_menu`;
CREATE TABLE `sysrole_menu`  (
  `role_id` smallint(6) UNSIGNED NOT NULL DEFAULT 0,
  `menu_id` smallint(6) UNSIGNED NOT NULL DEFAULT 0,
  `level` tinyint(1) NOT NULL DEFAULT 0,
  `pid` smallint(6) NOT NULL DEFAULT 0,
  `module` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  INDEX `groupId`(`role_id`) USING BTREE,
  INDEX `nodeId`(`menu_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysrole_menu
-- ----------------------------
INSERT INTO `sysrole_menu` VALUES (138, 97, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 142, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 143, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 144, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 145, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 146, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 6, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 85, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 86, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 87, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 88, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 89, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 90, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 91, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 92, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 203, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 7, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 150, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 151, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 152, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 153, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 154, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 30, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 168, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 169, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 173, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 143, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 144, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 145, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 146, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 6, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 85, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 86, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 87, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 88, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 89, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 90, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 91, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 92, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 203, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 7, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 150, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 151, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 152, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 153, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 154, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 30, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 168, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 169, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 173, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 174, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 175, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 188, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 84, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 149, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 155, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 158, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 159, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 160, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 148, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 157, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 156, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 163, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 164, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 187, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 161, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 182, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 183, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 184, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 185, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 186, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 192, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 170, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 198, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 199, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 174, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 175, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 7, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 150, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 151, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 152, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 188, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 84, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 149, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 155, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 158, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 159, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 200, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 201, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 152, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 151, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 150, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 149, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 148, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 188, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 187, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 164, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 163, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 161, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 160, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 159, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 158, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 157, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 156, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 155, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 154, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 153, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 84, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 30, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 172, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 199, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 198, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 203, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 143, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 7, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 202, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 160, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 148, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 157, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 156, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 163, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 164, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 187, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 161, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 182, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 183, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 184, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 185, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 186, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 192, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 170, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 198, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 199, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 200, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 201, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 202, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 172, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 7, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 150, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 151, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 152, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 153, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 154, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 30, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 188, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 84, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 149, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 155, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 158, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 159, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 160, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 148, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 157, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 156, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 163, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 164, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 187, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 161, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 182, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 183, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 184, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 185, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 186, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 192, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 170, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 198, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 199, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 172, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 177, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 178, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 179, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 180, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 181, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 176, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 190, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 191, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 193, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 189, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 195, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 196, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 197, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 194, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 205, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 206, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 172, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 177, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 178, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 179, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 180, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 181, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 176, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 190, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 191, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 193, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 189, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 195, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 196, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 197, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 194, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 205, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 206, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 207, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 204, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 153, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 154, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 30, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 188, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 84, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 149, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 155, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 158, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 159, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 160, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 148, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 157, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 156, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 163, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 164, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 187, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 161, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 198, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 199, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 200, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 201, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 202, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 172, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 205, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 206, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 177, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 178, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 179, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 180, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 181, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 176, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 190, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 191, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 193, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 189, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 195, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 196, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 197, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 194, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 205, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 206, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 207, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 204, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 142, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (6, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 207, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 204, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 207, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 204, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 96, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 95, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 94, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (138, 93, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 203, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 94, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (137, 93, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 203, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 94, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (136, 93, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 142, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 94, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (135, 93, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 151, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 94, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 92, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 7, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 150, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 151, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 152, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 153, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 154, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 30, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 169, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 188, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 84, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 149, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 155, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 158, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 159, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 160, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 288, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 148, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 157, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 156, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 275, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 276, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 208, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 260, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 261, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 209, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 210, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 247, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 248, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 211, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 270, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 212, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 266, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 213, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 240, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 241, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 242, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 243, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 244, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 245, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 143, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 6, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 92, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 7, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 150, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 151, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 152, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 153, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 154, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 30, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 168, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 169, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 173, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 174, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 175, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 188, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 84, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 149, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 155, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 158, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 159, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 160, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 288, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 148, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 157, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 156, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 163, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 164, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 187, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 161, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 198, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 199, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 200, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 201, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 202, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 172, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 274, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 275, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 276, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 277, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 278, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 279, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 208, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 259, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 260, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 261, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 262, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 263, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 264, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 209, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 252, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 253, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 254, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 255, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 256, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 257, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 258, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 210, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 246, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 247, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 248, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 249, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 250, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 251, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 211, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 269, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 270, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 271, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 272, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 273, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 289, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 290, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 212, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 265, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 266, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 267, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 268, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 213, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 280, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 281, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 282, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 283, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 284, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 285, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 286, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 287, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 214, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 219, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 215, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 231, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 232, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 233, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 234, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 235, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 236, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 237, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 238, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 239, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 216, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 225, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 220, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 221, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 222, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 223, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 224, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 218, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (140, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 226, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 227, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 228, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 152, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 153, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 154, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 30, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 149, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 155, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 158, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 159, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 160, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 288, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 148, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 157, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 156, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 275, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 260, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 253, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 247, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 270, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 212, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 266, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 240, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 241, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 242, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 215, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 231, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 232, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 236, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 237, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 238, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 216, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 225, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 229, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 230, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 226, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 227, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 228, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 229, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 230, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 217, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 217, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (139, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (141, 150, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (20, 109, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (20, 102, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (1, 167, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (1, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (1, 134, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (1, 139, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (1, 135, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (1, 102, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (1, 101, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (8, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (8, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (8, 3, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (8, 4, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (8, 40, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (8, 99, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (8, 100, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (8, 215, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (8, 216, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (9, 7, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (22, 134, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (22, 109, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (12, 168, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (12, 167, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 180, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 179, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 178, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 177, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 176, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 114, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 113, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 112, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 110, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 55, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 54, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 53, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 52, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 51, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 50, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 49, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 48, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 47, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 46, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 45, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 44, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 43, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 42, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 41, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 21, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 20, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 19, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 18, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 9, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 13, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 12, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 11, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 10, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 8, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 115, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 100, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 99, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 40, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 4, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 3, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (10, 5, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (12, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (12, 139, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (26, 142, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (11, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (11, 100, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (11, 99, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (11, 40, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (11, 4, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (11, 3, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (22, 105, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (22, 102, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (22, 101, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (22, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (2, 167, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (2, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (2, 140, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (2, 139, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (15, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (15, 5, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (19, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (17, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (17, 5, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (1, 168, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 87, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (5, 168, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (5, 167, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (5, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (5, 137, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (12, 108, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (2, 168, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (22, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (20, 101, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 55, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 54, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 53, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 52, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 51, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 42, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 41, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 39, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 38, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 37, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 35, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 21, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 20, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 19, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 18, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 9, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 13, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 12, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 11, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 10, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 8, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 115, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 16, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 15, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 14, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 7, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 6, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 254, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 142, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 100, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 99, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 40, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 256, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 4, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 3, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 5, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (13, 256, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (13, 4, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (21, 256, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (21, 4, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (21, 3, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (26, 100, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (26, 99, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (26, 40, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (26, 3, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (26, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (26, 5, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (11, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (25, 3, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (24, 3, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (19, 5, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (21, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (21, 5, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (20, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (18, 4, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (16, 99, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (16, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (13, 3, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (13, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (5, 136, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (5, 134, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (5, 139, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (5, 135, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (5, 102, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (5, 101, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (12, 105, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (2, 108, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (2, 105, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (2, 104, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (12, 104, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (12, 102, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (12, 101, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (12, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (2, 102, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (2, 101, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (2, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (24, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (24, 5, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (13, 5, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 189, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 188, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 187, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 186, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 190, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 127, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 126, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 125, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 124, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 159, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 123, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 122, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 121, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 120, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 119, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 118, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 117, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 2, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 116, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 70, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 69, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 68, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 67, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 66, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 65, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 64, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 63, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 62, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 61, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 34, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 33, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 32, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 31, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 30, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 29, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 28, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 171, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 168, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 167, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 137, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 136, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 134, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 140, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 139, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 135, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 108, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 107, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 180, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 179, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 178, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 177, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 176, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 144, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 143, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 141, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 113, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 112, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 110, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 55, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 53, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 52, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 51, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (7, 1, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 89, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 90, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 110, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 112, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 113, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 141, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 143, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 144, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 176, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 178, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 179, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 180, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 101, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 102, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 105, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 109, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 134, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 165, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 169, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 210, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 211, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 212, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 213, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (23, 214, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (27, 115, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (27, 16, 0, 0, NULL);
INSERT INTO `sysrole_menu` VALUES (27, 15, 0, 0, NULL);

-- ----------------------------
-- Table structure for sysrole_user
-- ----------------------------
DROP TABLE IF EXISTS `sysrole_user`;
CREATE TABLE `sysrole_user`  (
  `user_id` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `role_id` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  UNIQUE INDEX `uid_group_id`(`user_id`, `role_id`) USING BTREE,
  INDEX `uid`(`user_id`) USING BTREE,
  INDEX `group_id`(`role_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色用户表' ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of sysrole_user
-- ----------------------------
INSERT INTO `sysrole_user` VALUES (139, 2);
INSERT INTO `sysrole_user` VALUES (139, 12);
INSERT INTO `sysrole_user` VALUES (148, 1);
INSERT INTO `sysrole_user` VALUES (149, 2);
INSERT INTO `sysrole_user` VALUES (152, 2);
INSERT INTO `sysrole_user` VALUES (155, 1);
INSERT INTO `sysrole_user` VALUES (158, 2);
INSERT INTO `sysrole_user` VALUES (164, 2);
INSERT INTO `sysrole_user` VALUES (166, 20);
INSERT INTO `sysrole_user` VALUES (166, 22);
INSERT INTO `sysrole_user` VALUES (167, 12);
INSERT INTO `sysrole_user` VALUES (168, 12);
INSERT INTO `sysrole_user` VALUES (169, 12);
INSERT INTO `sysrole_user` VALUES (170, 12);
INSERT INTO `sysrole_user` VALUES (173, 12);
INSERT INTO `sysrole_user` VALUES (175, 7);
INSERT INTO `sysrole_user` VALUES (176, 12);
INSERT INTO `sysrole_user` VALUES (177, 1);
INSERT INTO `sysrole_user` VALUES (177, 2);
INSERT INTO `sysrole_user` VALUES (177, 6);
INSERT INTO `sysrole_user` VALUES (178, 7);
INSERT INTO `sysrole_user` VALUES (180, 12);
INSERT INTO `sysrole_user` VALUES (181, 12);
INSERT INTO `sysrole_user` VALUES (182, 12);
INSERT INTO `sysrole_user` VALUES (183, 12);
INSERT INTO `sysrole_user` VALUES (185, 2);
INSERT INTO `sysrole_user` VALUES (191, 2);
INSERT INTO `sysrole_user` VALUES (192, 2);
INSERT INTO `sysrole_user` VALUES (194, 2);

-- ----------------------------
-- Table structure for sysuser
-- ----------------------------
DROP TABLE IF EXISTS `sysuser`;
CREATE TABLE `sysuser`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fenlei` char(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `bind_account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_login_time` int(11) UNSIGNED NULL DEFAULT 0,
  `last_login_ip` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bind_p` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `login_count` mediumint(8) UNSIGNED NULL DEFAULT 0,
  `verify` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sex` tinyint(2) NULL DEFAULT NULL,
  `mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `status` char(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '可用' COMMENT '可用   禁用',
  `type_id` tinyint(2) UNSIGNED NULL DEFAULT 0,
  `info` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `datatotal` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '否' COMMENT '是 否',
  `isdatayuyue` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '否' COMMENT '是 否',
  `isdatayuyuetotal` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '否' COMMENT '是 否',
  `isdatatanan` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '否' COMMENT '是 否',
  `department_id` int(11) NULL DEFAULT NULL,
  `otherdepartment_ids` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `chuangwei_id` int(11) NULL DEFAULT NULL,
  `sushe_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `account`(`account`) USING BTREE,
  INDEX `nickname`(`nickname`) USING BTREE,
  INDEX `last_login_time`(`last_login_time`) USING BTREE,
  INDEX `mobile`(`mobile`) USING BTREE,
  INDEX `deptid`(`department_id`) USING BTREE,
  INDEX `chuangweiid`(`chuangwei_id`) USING BTREE,
  INDEX `susheid`(`sushe_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 184 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysuser
-- ----------------------------
INSERT INTO `sysuser` VALUES (139, '管理', 'admin', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '', 1566778366, '127.0.0.1', '123456', 840, NULL, 1, '15643132', '222@qq.com', '沃尔特沃尔特', NULL, '2020-07-25 13:42:14', '可用', 0, '', '是', '是', '是', '是', 1, '1,17,12', NULL, NULL);
INSERT INTO `sysuser` VALUES (166, '运营', '王三', '王三', 'e10adc3949ba59abbe56e057f20f883e', '', 1434508189, '222.90.140.101', '123456', 56, NULL, 1, '15643132', '445512@qq.com', '', NULL, '2020-05-30 10:43:37', '可用', 0, '', '否', '是', '是', '否', 27, '1,12,17,24,20,25,3,27,28', NULL, NULL);
INSERT INTO `sysuser` VALUES (167, '客服', '高铭', '高铭', 'e10adc3949ba59abbe56e057f20f883e', '', 1501125438, '1.80.1.109', NULL, 110, NULL, 0, '15643132', '45663@qq.com', '', NULL, '2020-05-30 10:49:18', '可用', 0, '', '否', '是', '否', '否', 28, '28', NULL, NULL);
INSERT INTO `sysuser` VALUES (168, '客服', '黄少', '黄少', 'e10adc3949ba59abbe56e057f20f883e', '', 1501828892, '113.140.251.109', NULL, 16, NULL, 0, '15643132', 'fffff@qq.com', '', NULL, '2020-05-30 10:48:34', '可用', 0, '', '否', '是', '否', '否', 28, '28', NULL, NULL);
INSERT INTO `sysuser` VALUES (169, '客服', '王都', '王都', 'e10adc3949ba59abbe56e057f20f883e', '', 1505273712, '36.47.160.38', '123456', 149, NULL, 0, '15643132', '111@qq.com', '', NULL, '2020-05-30 10:51:13', '可用', 0, '', '否', '是', '否', '否', 28, '28', NULL, NULL);
INSERT INTO `sysuser` VALUES (170, '客服', '葛立帆', '葛立帆', 'e10adc3949ba59abbe56e057f20f883e', '', 1507951452, '1.80.3.254', NULL, 770, NULL, 0, '15643132', '王芬@qq.com', '', NULL, '2020-05-30 10:51:01', '可用', 0, '', '否', '是', '否', '否', 28, '28', NULL, NULL);
INSERT INTO `sysuser` VALUES (173, '客服', '李楠', '李楠', 'e10adc3949ba59abbe56e057f20f883e', '额大热天', 0, NULL, NULL, 0, NULL, 1, '儿童', '冯密@qq.com', '', NULL, '2020-05-30 10:52:00', '可用', 0, NULL, '否', '是', '否', '否', 28, '28', NULL, NULL);
INSERT INTO `sysuser` VALUES (175, '运营', '阿斯蒂芬', '阿斯蒂芬阿', 'e10adc3949ba59abbe56e057f20f883e', '额大热天人', 0, NULL, NULL, 0, NULL, 0, '阿斯蒂芬阿斯蒂芬阿斯蒂芬', '阿斯蒂芬阿斯蒂芬阿斯蒂芬', '阿斯利康代煎放案例开始就发', NULL, '2021-07-01 17:23:25', '0', 0, NULL, '否', '是', '是', '否', 32, '1,12,17,24,20,25,3,27,28,19,21,22,23,26,31,29,32,33', NULL, NULL);
INSERT INTO `sysuser` VALUES (176, '客服', '王化', '王化', 'e10adc3949ba59abbe56e057f20f883e', '是大法官', 0, NULL, '123456', 0, NULL, 0, '是', '123456@qq.com', '', NULL, '2020-05-30 11:16:12', '可用', 0, NULL, '否', '是', '否', '否', 28, '28', NULL, NULL);
INSERT INTO `sysuser` VALUES (177, '运营', '发', '发', 'e10adc3949ba59abbe56e057f20f883e', '发', 0, NULL, '123456', 0, NULL, NULL, NULL, '45555@qq.com', '', NULL, '2020-05-29 16:30:12', '禁用', 0, NULL, '否', '否', '否', '否', 17, '17,24,20', NULL, NULL);
INSERT INTO `sysuser` VALUES (178, '运营', '人事总管理', '人事总管理', 'e10adc3949ba59abbe56e057f20f883e', '王博', 0, NULL, '123456', 0, NULL, NULL, NULL, '123@qq.com', '', NULL, '2020-11-13 10:20:06', '可用', 0, NULL, '否', '否', '否', '否', 1, '1,12,17,24,20,25,3,27,28,19,21,22,23,26,31,29,32,33', NULL, NULL);
INSERT INTO `sysuser` VALUES (179, '运营', '王姜', '王姜', 'e10adc3949ba59abbe56e057f20f883e', '王博1', 0, NULL, '123456', 0, NULL, NULL, '', '123@qq.com', '', NULL, '2021-07-02 15:19:32', '可用', 0, NULL, '是', '是', '是', '否', 17, '1,12,17,24,20,25,3,27,28,19,21,22,23,26,31,29,32,33', NULL, NULL);
INSERT INTO `sysuser` VALUES (180, '客服', '王行', '王行', 'e10adc3949ba59abbe56e057f20f883e', '王行行', 0, NULL, '123456', 0, NULL, NULL, NULL, '123@qq.com', '', NULL, NULL, '可用', 0, NULL, '否', '是', '否', '否', 28, '28', NULL, NULL);
INSERT INTO `sysuser` VALUES (181, '客服', '崔卫涛', '崔卫涛', 'e10adc3949ba59abbe56e057f20f883e', '崔卫涛', 0, NULL, '123456', 0, NULL, NULL, NULL, '123@qq.com', '', NULL, NULL, '可用', 0, NULL, '否', '是', '否', '否', 28, '28', NULL, NULL);
INSERT INTO `sysuser` VALUES (182, '客服', '刘艺星', '刘艺星', 'e10adc3949ba59abbe56e057f20f883e', '刘艺星', 0, NULL, '123456', 0, NULL, NULL, NULL, '123@qq.com', '', NULL, NULL, '可用', 0, NULL, '否', '是', '否', '否', 28, '28', NULL, NULL);
INSERT INTO `sysuser` VALUES (183, '客服', '张倩', '张倩', 'e10adc3949ba59abbe56e057f20f883e', '张倩', 0, NULL, '123456', 0, NULL, NULL, NULL, '123@qq.com', '', NULL, NULL, '可用', 0, NULL, '否', '是', '否', '否', 28, '28', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
