<?php

namespace app\common\controller;

use app\BaseController;
use app\common\utils\Jump;
use think\App;
use think\exception\HttpResponseException;
use think\facade\Request;
use think\facade\View;

class CommonController extends BaseController
{
    protected $isPost;
    protected $isGet;
    protected $isAjax;
    protected $appName;
    protected $moduleName;
    protected $controllerName;
    protected $actionName;

    protected function initialize()
    {
        $this->isPost = request()->isPost();
        $this->isGet = request()->isGet();
        $this->isAjax = request()->isAjax();
        $this->moduleName = strtolower(app('http')->getName());
        $this->controllerName = strtolower(app('request')->controller());
        $this->actionName = strtolower(app('request')->action());
        parent::initialize(); // TODO: Change the autogenerated stub
    }

    /**
     * 模板赋值
     * @param mixed ...$vars
     */
    protected function assign(...$vars)
    {
        View::assign(...$vars);
    }

    /**
     * 解析和获取模板内容
     * @param string $template
     * @return string
     * @throws \Exception
     */
    protected function fetch(string $template = '')
    {
        return View::fetch($template);
    }
    use Jump;
}