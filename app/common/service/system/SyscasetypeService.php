<?php

namespace app\common\service\system;

use app\admin\model\system\SyscasetypeModel;
use think\Service;

class SyscasetypeService extends Service
{
    public static function getAllList()
    {
        $model = new SyscasetypeModel();
        $model = $model->where("status", '可用')->order('id', 'asc')->select();
        // var_dump(Db::getLastSql());
        return $model;
    }

    public static function getListInCase()
    {
        $model = new SyscasetypeModel();

        $res = $model->alias('a')->field('a.id,a.name')
                     ->join(['sysproject_casetype' => 'pc'], 'a.id=pc.casetype_id')
                     ->where('pc.project_id', '=', session('projectid'))
                     ->select();
        // var_dump(Db::getLastSql());
        return $res;
    }
}