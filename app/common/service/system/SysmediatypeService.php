<?php

namespace app\common\service\system;

use app\admin\model\system\SysdepartmentMediatypeModel;
use app\admin\model\system\SysmediatypeModel;
use app\admin\model\system\SysprojectMediatypeModel;
use think\Service;

class SysmediatypeService extends Service
{
    public static function getAllList()
    {
        $model = new SysmediatypeModel();
        return $model->where([])->order('id', 'asc')->select();
    }

    public static function getListInProjectAndDepartment()
    {
        $ids_project = self::getIdsInProject(session('projectid'));
        $ids_department = self::getIdsInDepartment(session('sysuser.department_id'));
        $ids=array_intersect($ids_project, $ids_department);
        $res=SysmediatypeModel::yqGetAllData([['id','in',$ids]]);
        // halt($res);
        return $res;
    }

    public static function getIdsInProject($project_id)
    {
        return SysprojectMediatypeModel::yqGetColumn([['project_id', '=', $project_id]], 'mediatype_id');
    }

    public static function getIdsInDepartment($department_id)
    {
        return SysdepartmentMediatypeModel::yqGetColumn([['department_id', '=', $department_id]], 'mediatype_id');
    }
}