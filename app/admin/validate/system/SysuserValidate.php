<?php

namespace app\admin\validate\system;

use think\Validate;

class SysuserValidate extends Validate
{
    protected $rule = [
        'account'  => 'require|max:255|unique:sysuser',
        // 'account'  => 'unique:sysuser',
        'nickname' => 'require|max:255',
    ];
    protected $message = [
        'account.require'  => '账户必须！',
        'account.max'      => '账户名称最多不能超过255个字符！',
        'account.unique'   => '账户名称已经存在！',
        'nickname.require' => '昵称必须！',
        'nickname.max'     => '昵称名称最多不能超过255个字符！',
    ];
}