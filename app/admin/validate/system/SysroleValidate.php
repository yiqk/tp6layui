<?php

namespace app\admin\validate\system;

use think\Validate;

class SysroleValidate extends Validate
{
    protected $rule = [
        'title'  => 'require|max:255|',
        'title'  => 'unique:sysrole',
    ];
    protected $message = [
        'title.require' => '名称必须！',
        'title.max'     => '名称最多不能超过255个字符！',
        'title.unique'  => '角色名称已经存在！',
    ];
}