<?php

namespace app\admin\model\system;

use app\common\model\CommonModel;

class SysdepartmentModel extends CommonModel
{
    protected $name = "sysdepartment";
    protected $autoWriteTimestamp = 'datetime';

    public function users()
    {
        return $this->hasMany(SysuserModel::class, 'department_id');
    }

    public function mediatypes()
    {
        return $this->belongsToMany(SysmediatypeModel::class, 'sysdepartment_mediatype', 'mediatype_id', 'department_id');
    }

    public function medias()
    {
        return $this->belongsToMany(SysmediaModel::class, 'sysdepartment_media', 'media_id', 'department_id');
    }

    public static function onBeforeInsert($model)
    {
        $model->project_id = session('projectid');
        // $model->project_name = session('projectname');
    }

    public function getIndexData()
    {
        $page = input('page');
        $pageSize = input('pageSize');
        $sort = input('sort');
        $order = input('order');
        $where = array();
        // $account = input('account');
        // $where[] = ['name', 'like', '%' . $account . '%'];
        $where[] = ['project_id', '=', session('projectid')];
        $model = $this->where($where)->with(["users", "mediatypes", "medias"])->order("id", "asc")->select();
        // dump($this->getLastSql());
        return $model;
    }
}