<?php

namespace app\admin\model\system;

use app\common\model\CommonModel;

class SysprojectMediaModel extends CommonModel
{
    protected $name = "sysproject_media";
    protected $autoWriteTimestamp = 'datetime';
}