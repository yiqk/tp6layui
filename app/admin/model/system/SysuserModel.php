<?php

namespace app\admin\model\system;

use app\common\model\CommonModel;

class SysuserModel extends CommonModel
{
    protected $name = "sysuser";

    public function department()
    {
        return $this->belongsTo(SysdepartmentModel::class, 'department_id')->bind([
            'departmentname' => 'name',
        ]);
    }

    public function roles()
    {
        return $this->belongsToMany(SysroleModel::class, 'sysrole_user', 'role_id', 'user_id');
    }

    public function projects()
    {
        return $this->belongsToMany(SysprojectModel::class, 'sysproject_user', 'project_id', 'user_id');
    }

    public function getIndexData()
    {
        $page = input('page');
        $pageSize = input('pageSize');
        $sort = input('sort');
        $order = input('order');
        $where = array();
        $account = input('account');
        if (!empty($account)) {
            $where[] = ['account', 'like', '%' . $account . '%'];
        }
        $department_id = input('department_id');
        if (!empty($department_id)) {
            $where[] = ['department_id', '=', $department_id];
        }
        // $where[] = ['status', '=', '可用'];
        $model = $this->where($where)->with(["roles", "projects", "department"])->order($sort, $order)->paginate($pageSize);
        // dump($this->getLastSql());
        return $model;
    }
}