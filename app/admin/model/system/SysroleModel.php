<?php

namespace app\admin\model\system;

use app\common\model\CommonModel;

class SysroleModel extends CommonModel
{
    protected $name = "sysrole";

    public function menus()
    {
        return $this->belongsToMany(SysmenuModel::class, 'sysrole_menu', 'menu_id', 'role_id');
    }
    
    public function users()
    {
        return $this->belongsToMany(SysuserModel::class, 'sysrole_user', 'user_id', 'role_id');
    }
}