<?php

namespace app\admin\model\system;

use app\common\model\CommonModel;

class SysmediaModel extends CommonModel
{
    protected $name = "sysmedia";
    protected $autoWriteTimestamp = 'datetime';

    public function mediatype()
    {
        return $this->belongsTo(SysmediatypeModel::class, 'mediatype_id')->bind([
            'mediatypename' => 'name',
        ]);;
    }

    public function getIndexData()
    {
        $page = input('page');
        $pageSize = input('pageSize');
        $sort = input('sort');
        $order = input('order');
        $where = array();
        $name = input('name');
        $mediatype_id = input('mediatype_id');
        if (!empty($name)) {
            $where[] = ['name', 'like', '%' . $name . '%'];
        }
        if (!empty($mediatype_id)) {
            $where[] = ['mediatype_id', '=', $mediatype_id];
        }
        $modellist = $this->where($where)->with(['mediatype'])->order("mediatype_id", "asc")->paginate($pageSize);
        // dump($this->getLastSql());
        return $modellist;
    }
}