<?php

namespace app\admin\model\system;

use app\common\model\CommonModel;

class SyslogModel extends CommonModel
{
    protected $name = "syslog";

    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }
}