<?php

namespace app\admin\controller\system;

use app\admin\controller\AuthController;
use app\admin\controller\WidgetController;
use app\admin\model\system\SysmediaModel;
use app\admin\model\system\SysmenuModel;
use app\admin\model\system\SysprojectModel;
use app\admin\validate\system\SysprojectValidate;
use app\common\service\system\SyscasetypeService;
use app\common\service\system\SysdepartmentService;
use app\common\service\system\SysmediaService;
use think\Db;

class SysprojectController extends AuthController
{
    public function index()
    {
        if (!$this->isPost) {
            return $this->fetch();
        } else {
            $model = new SysprojectModel();
            $list = $model->getIndexData();
            return json(['rows' => $list->toArray()['data'], 'total' => $list->total()]);
            // return $list;
        }
    }

    public function add()
    {
        if ($this->isPost) {
            $params = input('post.');
            unset($params['id']);
            $casetypeids = input('post.casetypeids');
            $mediaids = input('post.mediaids');
            if (empty($casetypeids)) {
                $this->error("请选择类型！", "", $params);
                exit;
            }
            if (empty($mediaids)) {
                $this->error("请选择渠道来源！", "", $params);
                exit;
            }
            $validate = new SysprojectValidate();
            if (!$validate->check($params)) {
                $this->error($validate->getError());
                exit;
            }
            try {
                $res = SysprojectModel::yqCreate($params);
                $model = SysprojectModel::find($res->id);
                $model->medias()->detach();//删除关联中间表数据
                $model->medias()->saveAll($mediaids);
                $model->casetypes()->detach();//删除关联中间表数据
                $model->casetypes()->saveAll($casetypeids);
                $map[] = ['id', 'in', $mediaids];
                $tempids = SysmediaModel::where($map)->column('mediatype_id');
                $model->mediatypes()->detach();//删除关联中间表数据
                $model->mediatypes()->saveAll(array_unique($tempids));
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            $this->success("成功！", "", $params);
        } else {
            return $this->fetch();
        }
    }

    public function edit()
    {
        if ($this->isPost) {
            $params = input('post.');
            $casetypeids = input('post.casetypeids');
            $mediaids = input('post.mediaids');
            if (empty($casetypeids)) {
                $this->error("请选择类型！", "", $params);
                exit;
            }
            if (empty($mediaids)) {
                $this->error("请选择渠道来源！", "", $params);
                exit;
            }
            SysprojectModel::update($params);
            $model = SysprojectModel::find($params['id']);
            $model->medias()->detach();//删除关联中间表数据
            $model->medias()->saveAll($mediaids);
            $model->casetypes()->detach();//删除关联中间表数据
            $model->casetypes()->saveAll($casetypeids);
            $map[] = ['id', 'in', $mediaids];
            $tempids = SysmediaModel::where($map)->column('mediatype_id');
            $model->mediatypes()->detach();//删除关联中间表数据
            $model->mediatypes()->saveAll(array_unique($tempids));
            $this->success("成功！", "", $params);
        } else {
            $this->assign('casetypelist', SyscasetypeService::getAllList());
            $this->assign('medialist', SysmediaService::getAllList());
            return $this->fetch();
        }
    }

    public function delete()
    {
        $ids = input('ids');
        $res = SysprojectModel::yqDeleteByIds($ids);
        $this->success("成功！", "", $res);
    }
}
